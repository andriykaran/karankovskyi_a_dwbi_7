USE labor_sql
GO

-- Task 1
;WITH company_1_trip AS
(
SELECT * FROM trip WHERE id_comp = 1
)

SELECT * FROM company_1_trip WHERE town_from = 'Rostov';
GO

--Task 2
;WITH 
company_5_trip AS
(
SELECT * FROM trip WHERE id_comp = 5    
),
company_5_London AS 
(
SELECT * FROM company_5_trip WHERE town_from = 'London'
)

SELECT * FROM company_5_London;
GO

--Task 3
SELECT region_id, id, name, (SELECT TOP 1 region_id FROM geography WHERE region_id = 1) AS PlaceLevev FROM geography WHERE region_id = 1;
GO

--Task 4
SELECT region_id, id, name, 0 AS PlaceLevev FROM geography 
WHERE region_id = 4
UNION
SELECT region_id, id, name, 1 AS PlaceLevev FROM geography 
WHERE region_id = 21
GO

--Task 5
;WITH
  CTE_10000(number) AS 
  (
	SELECT number = 1 
	UNION ALL
	SELECT number + 1 FROM CTE_10000 WHERE number < 10000
  )
SELECT * FROM CTE_10000
OPTION (maxrecursion 0);

--Task 6
;WITH
  CTE_100000(number) AS 
  (
	SELECT number = 1 
	UNION ALL
	SELECT number + 1 FROM CTE_100000 WHERE number < 100000
  )
SELECT * FROM CTE_100000
OPTION (maxrecursion 0);
GO

--Task 7
;WITH 
	CTE_Sn AS
	(
		SELECT number = 1, day ='��' 
		UNION ALL
		SELECT number + 7, '��' FROM CTE_Sn WHERE number < 357			
	),
		CTE_St AS
	(		
		SELECT number = 2, day ='��' 
		UNION ALL
		SELECT number + 7, '��' FROM CTE_St WHERE number < 357
	)

SELECT Count(day), 'ʳ������ �����' FROM CTE_Sn WHERE number < 357
UNION ALL
SELECT Count(day), 'ʳ������ �����' FROM CTE_St WHERE number < 357
GO

--Task 8
SELECT DISTINCT maker FROM product 
where
	(type = 'pc') 
	AND
	( maker NOT IN (SELECT maker FROM product where type = 'laptop'))
GO
--Task 9
SELECT DISTINCT maker FROM product 
where
	(type = 'pc') 
	AND
	( maker <> ALL (SELECT maker FROM product where type = 'laptop'))
GO

--Task 10
SELECT DISTINCT maker FROM product 
where
	(type = 'pc') 
	AND NOT
	(maker = ANY (SELECT maker FROM product where type = 'laptop'))
GO
--Task 11
SELECT DISTINCT maker FROM product 
where
	(type = 'pc') 
	AND
	( maker IN (SELECT maker FROM product where type = 'laptop'))
GO

--Task 12
SELECT DISTINCT maker FROM product 
where
	(type = 'pc') 
	AND NOT
	( maker != ALL (SELECT DISTINCT maker FROM product where type = 'laptop'))
GO

--Task 13
SELECT DISTINCT maker FROM product 
where
	(type = 'pc') 
	AND
	(maker = ANY (SELECT maker FROM product where type = 'laptop'))
GO

--Task 14
SELECT DISTINCT maker FROM product
WHERE 
	type = 'pc'
EXCEPT
SELECT DISTINCT maker FROM product
WHERE 
	type = 'pc'
	AND
	maker != ALL (SELECT maker FROM pc WHERE product.model = pc.model)
go

--Task 15
SELECT country, class FROM classes
WHERE country = 'Ukraine' 
UNION 
SELECT country, class FROM classes
WHERE NOT EXISTS (SELECT country, class FROM classes
WHERE country = 'Ukraine') ORDER BY country
GO

--Task 16
SELECT ship, battle, result FROM outcomes
WHERE 
	result = 'damaged'
	AND 
	ship IN (SELECT ship FROM outcomes GROUP BY ship HAVING COUNT(result) > 1);
GO

--Task 17
SELECT DISTINCT maker FROM product
WHERE 
	type = 'pc'
	AND
	EXISTS (SELECT maker FROM pc WHERE product.model = pc.model)
EXCEPT
SELECT DISTINCT maker FROM product
WHERE 
	type = 'pc'
	AND
	NOT EXISTS (SELECT maker FROM pc WHERE product.model = pc.model)
GO

--Task 18
SELECT DISTINCT maker FROM product
WHERE model IN (SELECT model FROM pc WHERE speed = (SELECT MAX(speed) FROM pc ))
Go

--Task 19
SELECT class FROM ships WHERE name IN (
	SELECT ship FROM outcomes WHERE result = 'sunk'
	)
UNION 
SELECT class FROM classes WHERE class IN (
	SELECT ship FROM outcomes WHERE result = 'sunk'
	)
GO

--Task 20
SELECT model, price FROM printer
WHERE price = (SELECT MAX(price) FROM printer)
GO

--Task 21
SELECT (SELECT DISTINCT type FROM product WHERE type = 'laptop') AS type, model, speed FROM laptop
WHERE speed < ALL (SELECT speed FROM pc)
GO

--Task 22
SELECT maker, price FROM printer JOIN product ON product.model = printer.model
WHERE color = 'y' AND printer.price = (SELECT min(price) FROM printer WHERE color = 'y')

--Task 23
SELECT battle, country, count(ship) AS number_of_ships FROM (
	SELECT battle, country, ship FROM outcomes JOIN ships ON outcomes.ship = ships.name JOIN classes ON ships.class = classes.class
	) a
GROUP BY battle, country
HAVING count(ship) >= 2
GO

--Task 24
SELECT maker, SUM(pc) AS pc, SUM(printer) AS printer, SUM(laptop) AS laptop FROM (
	SELECT maker, count(pc_model) AS pc, count(printer_model) AS printer, count(laptop_model) AS laptop FROM (
		SELECT 
			maker, 
			product.model,
			product.type,
			pc.model AS pc_model,
			printer.model AS printer_model,
			laptop.model AS laptop_model
		FROM product 
			LEFT JOIN pc ON product.model = pc.model 
			LEFT JOIN laptop ON product.model = laptop.model
			LEFT JOIN printer ON product.model = printer.model
		) sub1
	GROUP BY type, maker
) sub2 
GROUP BY maker

--Task 25
	SELECT
		maker,
		CASE WHEN count(pc_model) > 0 
		THEN CONCAT('yes(',count(pc_model), ')')
		ELSE 'No'
		END	AS pc 
	FROM (
		SELECT 
			maker, 
			product.model,			
			pc.model AS pc_model			
		FROM product 
			LEFT JOIN pc ON product.model = pc.model			
		) sub
	GROUP BY maker

--Task 26
SELECT 
	ISNULL(outcome_o.point, income_o.point) AS point,
	ISNULL(outcome_o.date, income_o.date) AS date,
	inc,
	out
FROM income_o FULL JOIN outcome_o ON income_o.point = outcome_o.point AND income_o.date = outcome_o.date

--Task 27
SELECT name, numGuns, bore, displacement, type, country, launched, ships.class FROM ships LEFT JOIN classes ON ships.class = classes.class
WHERE numGuns=8 OR bore=15 OR displacement=32000 OR type='bb' OR country='USA' OR launched=1915 OR ships.class='Kongo'

--Task 28
SELECT 
	isNULL(outcome.point, sub.point) AS point,
	isNull(outcome.date, sub.date) AS date,
	CASE 
		WHEN ISNULL(outcome.out, 0) > ISNULL(sub.out, 0) THEN 'once a day'
		WHEN ISNULL(outcome.out, 0) < ISNULL(sub.out, 0) THEN 'more than once a day'
		WHEN ISNULL(outcome.out, 0) = ISNULL(sub.out, 0) THEN 'both'
	END AS winer	 
FROM outcome FULL JOIN 
(SELECT point, date, SUM(out) AS out FROM outcome_o GROUP BY point, date) sub
ON outcome.point = sub.point AND outcome.date = sub.date

--Task 29
SELECT maker, product.model, product.type, ISNULL(pc.price, ISNULL(laptop.price, printer.price)) AS prise
FROM 
	product LEFT JOIN pc ON product.model = pc.model 
	LEFT JOIN laptop ON product.model = laptop.model
	LEFT JOIN printer ON product.model = printer.model
WHERE maker = 'B'

--Task 30
SELECT name, class FROM ships WHERE name = class
UNION 
(SELECT ship, NUll FROM outcomes
EXCEPT
SELECT name, Null FROM ships) 

--Task 31
SELECT class, COUNT(class) AS Number_of_ships 
FROM ships 
GROUP BY class
HAVING COUNT(class) = 1
UNION 
(SELECT DISTINCT ship, 1 FROM outcomes
EXCEPT
SELECT name, 1 FROM ships) 

--Task 32
SELECT name FROM ships WHERE launched < 1942
UNION
SELECT ship FROM outcomes JOIN battles ON outcomes.battle = battles.name WHERE YEAR(date) < 1942


 