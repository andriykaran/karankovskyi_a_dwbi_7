USE labor_sql
go

--SELECT 1
SELECT maker, type, speed, hd FROM product JOIN pc ON product.model=pc.model
WHERE hd <= 8;
go
--SELECT 2
SELECT DISTINCT maker FROM product JOIN pc ON product.model=pc.model
WHERE speed >= 600;
go

--SELECT 3
SELECT DISTINCT maker FROM product JOIN laptop ON product.model=laptop.model
WHERE speed <= 500;
go
--SELECT 4
SELECT P1.model, P2.model, P1.hd, P1.ram  FROM laptop P1, laptop P2
WHERE P1.ram = P2.ram AND P1.hd = P2.hd AND P1.model > P2.model;
go
--SELECT 5
select country
from classes
group by country
having count(distinct type) = 2
go
--SELECT 6
SELECT DISTINCT maker, product.model FROM product JOIN pc ON product.model = pc.model
WHERE price < 600
go
--SELECT 7
SELECT DISTINCT maker, product.model FROM product JOIN printer ON product.model = printer.model
WHERE price > 300
go
--SELECT 8
SELECT maker, product.model, price FROM product JOIN pc ON product.model = pc.model
go
--SELECT 9
SELECT DISTINCT maker, product.model, price FROM product JOIN pc ON product.model = pc.model
go
--SELECT 10
SELECT maker, type, product.model, speed FROM product JOIN laptop ON product.model = laptop.model
WHERE speed > 600;
--SELECT 11
SELECT name, displacement FROM ships JOIN classes ON ships.class = classes.class
go
--SELECT 12
SELECT ship, battle, date FROM battles JOIN outcomes ON name = battle
WHERE result = 'OK';
go 
--SELECT 13
SELECT name, country FROM ships JOIN classes ON ships.class = classes.class
go
--SELECT 14
SELECT DISTINCT plane, name FROM trip JOIN company ON trip.id_comp = company.id_comp
WHERE plane = 'Boeing'
GO
--SELECT 15
SELECT name, date FROM passenger JOIN pass_in_trip ON passenger.id_psg = pass_in_trip.id_psg
GO
--SELECT 16
SELECT pc.model, speed, hd FROM pc JOIN product ON pc.model = product.model
WHERE hd IN (10, 20) AND maker = 'A'
ORDER BY speed
go
--SELECT 17
SELECT * FROM (select  maker,type from product) mt 
PIVOT( COUNT(type) FOR type IN (pc, laptop, printer)) AS maker_models_count
GO 

--SELECT 18 
SELECT * FROM (SELECT price, screen FROM laptop) AS sp
PIVOT (AVG(price) FOR screen IN ([11], [12], [14], [15]) ) AS lsp 
go

--SELECT 19
SELECT * FROM laptop
CROSS APPLY (SELECT maker from product 
WHERE laptop.model =  product.model) cp
go
--SELECT 20
SELECT * FROM laptop lt
CROSS APPLY (
				SELECT MAX(price) AS max_price from product JOIN laptop ON product.model = laptop.model 
				WHERE maker = (SELECT maker FROM product WHERE lt.model = product.model)) ca
go
--SELECT 21
SELECT * FROM laptop lt 
CROSS APPLY (
	SELECT TOP 1 * FROM laptop sl  
	WHERE lt.model < sl.model OR (lt.model = sl.model AND lt.code < sl.code)
	ORDER BY model, code
	) ca
ORDER BY lt.model, lt.code
go

--SELECT 22
SELECT * FROM laptop lt 
OUTER APPLY (
	SELECT TOP 1 * FROM laptop sl  
	WHERE lt.model < sl.model OR (lt.model = sl.model AND lt.code < sl.code)
	ORDER BY model, code
	) ca
ORDER BY lt.model, lt.code
go
--SELECT 23
SELECT * FROM (SELECT DISTINCT type FROM product) p
CROSS APPlY (
			select top 3* from product where p.type=type
			order by model) ca
go

--SELECT 24
SELECT code, name, value FROM Laptop
CROSS APPLY
(VALUES('speed', speed), ('ram', ram), ('hd', hd), ('screen', screen)) spec(name, value)
ORDER BY code, name, value
GO