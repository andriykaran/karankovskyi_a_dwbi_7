use [labor_sql];
go

--SELECT 1

SELECT DISTINCT maker, type FROM Product
WHERE type = 'Laptop' ORDER BY maker;
go

--SELECT 2
 SELECT model, ram, screen, price FROM Laptop
 WHERE price > 1000
 ORDER BY ram DESC, price
 go


--SELECT 3
SELECT * FROM Printer
WHERE color = 'y'
ORDER BY price DESC
go

--SELECT 4
SELECT model, speed, hd, cd, price FROM PC
WHERE cd IN ('12x', '24x') AND price < 600
go


--SELECT 5
SELECT name, class FROM ships
WHERE name = class
ORDER BY name
go

--SELECT 6
SELECT * FROM pc
WHERE speed >= 500 and price < 800
ORDER BY price DESC
go

--SELECT 7
SELECT * FROM printer
WHERE type != 'Matrix' AND price < 300
ORDER BY type
go


--SELECT 8
SELECT model, speed FROM  pc
WHERE price BETWEEN 400 AND 600
ORDER BY hd
go

 --SELECT 9
  SELECT model, speed, hd, price FROM laptop
  WHERE screen < 12
  ORDER BY price DESC
  go

--SELECT 10
SELECT model, type, price FROM printer
WHERE price < 300
ORDER BY type DESC
go
--SELECT 11
SELECT model, ram, price FROM laptop
WHERE ram  = 64
ORDER BY screen
go
--SELECT 12
SELECT model, ram, price FROM laptop
WHERE ram  = 64
ORDER BY hd
go

--SELECT 13
SELECT model, speed, price FROM pc
WHERE speed BETWEEN 500 AND 750
ORDER BY hd DESC
go
--SELECT 14
SELECT * FROM Outcome_o
WHERE out > 2000
ORDER BY date DESC
go

--SELECT 15
SELECT * FROM Income_o 
WHERE inc BETWEEN 5000 AND 10000
ORDER BY inc
go

--SELECT 16
SELECT * FROM Income
WHERE point = 1
ORDER BY inc
go

--SELECT 17
SELECT * FROM Outcome
WHERE point = 2
ORDER BY out
go

--SELECT 18
SELECT * FROM classes
WHERE country = 'Japan'
ORDER BY type DESC
go

--SELECT 19
SELECT name, launched FROM ships
WHERE launched BETWEEN 1920 AND 1942
ORDER BY launched DESC
go 

--SELECT 20
SELECT ship, battle, result FROM outcomes
WHERE battle = 'Guadalcanal' 
ORDER BY ship DESC
go


--SELECT 21
SELECT ship, battle, result FROM outcomes
WHERE result = 'sunk'
ORDER BY ship DESC
go

--SELECT 22
SELECT class, displacement FROM classes
WHERE displacement >= 40
ORDER BY type 
go


--SELECT 23
SELECT trip_no, town_from, town_to FROM trip
WHERE town_from = 'London' OR town_to = 'London'
ORDER BY time_out
go

--SELECT 24
SELECT trip_no, plane, town_from, town_to FROM trip
WHERE plane = 'TU-134'
ORDER BY time_out DESC
go

--SELECT 25
SELECT trip_no, plane, town_from, town_to FROM trip
WHERE plane = 'IL-86'
ORDER BY plane
go

--SELECT 26
SELECT trip_no, town_from, town_to FROM trip
WHERE town_from = 'Rostov' OR town_to = 'Rostov'
ORDER BY plane
go

--SELECT 27
SELECT model FROM pc
WHERE model like '%1%1%'
go

--SELECT 28

SELECT * FROM outcome
WHERE date BETWEEN '2001-03-01' AND '2001-03-31'
go

--SELECT 29
SELECT * FROM outcome_o
WHERE DAY(date) = 14
go


--SELECT 30
SELECT name FROM ships
WHERE name LIKE 'W%n'
go


--SELECT 31
SELECT name FROM ships
WHERE name LIKE '%E%E%'
go


--SELECT 32
SELECT name, launched FROM ships
WHERE name LIKE '%[^a]'
go


--SELECT 33
SELECT name FROM battles
WHERE name LIKE '% %[^c]'
go
 
--SELECT 34
SELECT * FROM trip
WHERE time_out BETWEEN '12:00' AND '17:00'
go

--SELECT 35
SELECT * FROM trip
WHERE time_in BETWEEN '17:00' AND '23:00'
go


--SELECT 36
SELECT * FROM trip
WHERE ((time_in > '21:00') AND (time_in < '23:59')) OR ((time_in > '00:00') AND (time_in < '10:00'))
go

--SELECT 37
SELECT date FROM pass_in_trip
WHERE place LIKE '1%'
go

--SELECT 38
SELECT date FROM pass_in_trip
WHERE place LIKE '%C'
go

--SELECT 39
SELECT RIGHT(RTRIM(name), (len(name) - CHARINDEX(' ', name))) AS surname FROM passenger
WHERE name LIKE '% C%'
go

--SELECT 40
SELECT RIGHT(RTRIM(name), (len(name) - CHARINDEX(' ', name))) AS surname FROM passenger
WHERE name NOT LIKE '% j%'
go

--SELECT 41
SELECT CONCAT('������� ���� = ', AVG(price)) AS AVGprice FROM laptop 
go

--SELECT 42
SELECT CONCAT('code: ', code) AS code, CONCAT('model: ', model) AS model, CONCAT('speed: ', speed) AS speed, CONCAT('ram: ', ram) AS ram, CONCAT('hd: ', hd) AS HD, CONCAT('cd: ',cd) AS CD, CONCAT('price ', price) AS Price FROM pc
go

--SELECT 43
SELECT CONVERT(varchar(10), [date], 102) AS DATE FROM income
go

--SELECT 44
SELECT ship, battle,
  CASE 
	WHEN result = 'sunk' THEN '�������'
	WHEN result = 'damaged' THEN '����������'
	ELSE '������������'
  END
	AS ���������
 FROM Outcomes
 go


--SELECT 45
SELECT CONCAT('���: ', left(place, 1)) AS ���, CONCAT('����: ', right(rtrim(place), 1)) AS ���� FROM pass_in_trip
go

--SELECT 46
SELECT trip_no, id_comp, plane, CONCAT('From ', trim(town_from), ' to ', trim(town_to)) AS ROUTE, time_out, time_in FROM trip
go

--SELECT 47
SELECT 
	CONCAT(
		LEFT(trip_no, 1),
		RIGHT(trip_no, 1),
		LEFT(id_comp, 1),
		RIGHT(id_comp, 1),
		LEFT(trim(plane), 1),
		RIGHT(trim(plane), 1),
		LEFT(trim(town_from), 1), 
		RIGHT(trim(town_from), 1),
		LEFT(trim(town_to), 1), 
		RIGHT(trim(town_to), 1),
		LEFT(time_out, 1), 
		RIGHT(time_out, 1),
		LEFT(time_in, 1), 
		RIGHT(time_in, 1)
	) 
FROM trip
go

--SELECT 48
SELECT maker, COUNT(model) AS [2 or more models ] FROM product
GROUP BY maker
HAVING COUNT(model) >= 2
go

--SELECT 49
SELECT 
	town_from AS town,
	COUNT(town_to) AS airplanes_fly_away,
	COUNT(time_in) AS planes_arrives
FROM trip
GROUP BY town_from 
go

--SELECT 50
SELECT type, COUNT(model) FROM printer
GROUP BY type
go

--SELECT 51
SELECT model, cd, COUNT(DISTINCT cd) AS [count_CD], COUNT(DISTINCT model) AS [COUNT_MODEL] FROM pc
GROUP BY GROUPING SETS (model, cd)

--SELECT 52
SELECT 
	time_out,
	time_in,
	CASE 
		WHEN time_out < time_in THEN CONVERT(VARCHAR(8),(time_in - time_out), 108)
		ELSE CONVERT(VARCHAR, (DATEADD(day, 1, time_in) - time_out ), 108)
	END
	AS flight_time
FROM trip
go

--SELECT 53
SELECT 
	point,
	date,
	SUM(out) AS DAY_SUM,
	MIN(out) AS min_out,
	MAX(out) AS max_out
FROM outcome
GROUP BY point, date  WITH ROLLUP
go

--SELECT 54
SELECT trip_no, LEFT(place, 1) AS row_numb, COUNT(*) AS busy_places FROM pass_in_trip
GROUP BY trip_no, LEFT(place, 1) WITH ROLLUP

SELECT * FROM pass_in_trip
ORDER BY trip_no, place
go

--SELECT 55
SELECT
 LEFT(RIGHT(RTRIM(name), (len(name) - CHARINDEX(' ', name))), 1) AS FIRST_LETER,
 Count(*)
FROM passenger
GROUP BY LEFT(RIGHT(RTRIM(name), (len(name) - CHARINDEX(' ', name))), 1)
HAVING (LEFT(RIGHT(RTRIM(name), (len(name) - CHARINDEX(' ', name))), 1) IN ('S', 'B', 'A'))
go
