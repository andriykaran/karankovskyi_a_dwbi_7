﻿use [module_3 − Andrii Karankovskyi]
go 

ALTER TABLE client_order 
DROP CONSTRAINT FK_client

drop TABLE if exists client
go


CREATE TABLE client(
	id int  NOT NULL PRIMARY KEY IDENTITY,
	name varchar(30) NOT NULL,
	surname varchar(20) NOT NULL,
	midle_name varchar(20),
	email varchar(50),
	age int, 
	gender varchar(1),
	city varchar(20),
	inserted_date datetime NOT NULL DEFAULT getdate(),
	updated_date datetime NOT NULL DEFAULT getdate(),
	CONSTRAINT name_surname_midle_name UNIQUE (name, surname, midle_name)) 
GO

DROP TABLE if exists [client_order]
go


CREATE TABLE [client_order] (
       id					int NOT NULL PRIMARY KEY IDENTITY,
       product_name       varchar(20) NOT NULL,
       product_description        varchar(200) NULL,
       price                decimal(10, 2) NOT NULL,
       quantity             int NOT NULL,
	   product_weight				float,
	   client_id			int NOT NULL,
	   day_of_shipment		date,
	   inserted_date		datetime NOT NULL DEFAULT getdate(),
		updated_date		datetime NOT NULL DEFAULT getdate(),
)
go

ALTER TABLE client 
ADD CONSTRAINT gender_check CHECK (gender IN ('F', 'M'))

ALTER TABLE client_order 
ADD CONSTRAINT FK_client 
FOREIGN KEY (client_id) REFERENCES client(id) 



/* while(exists(select 1 from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where CONSTRAINT_TYPE='FOREIGN KEY'))
begin
	declare @sql nvarchar(2000)
	SELECT TOP 1 @sql=('ALTER TABLE ' + TABLE_SCHEMA + '.[' + TABLE_NAME
	+ '] DROP CONSTRAINT [' + CONSTRAINT_NAME + ']')
	FROM information_schema.table_constraints
	WHERE CONSTRAINT_TYPE = 'FOREIGN KEY'
	exec (@sql)
end */