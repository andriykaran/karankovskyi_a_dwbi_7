﻿use [module_3 − Andrii Karankovskyi]
go

-- Insert corect data
INSERT INTO [module_3 − Andrii Karankovskyi].[dbo].[client] 
(name, surname, midle_name, email, age, gender, city)
VALUES
('Ivan', 'Sobol', Null, 'ivan@gmail.com', 27, 'M', 'Rivne'),
('Petro', 'Vovk', 'Semenovych', 'petro@gmail.com', 66, 'M', 'Korosten'),
('Maria', 'Zajec', 'Ivanivna', 'mari@ukr.net', 22, 'F', 'Kyiv'),
('Uljana', 'Lys', Null, 'uljana@gmail.com', 29, 'F', 'Lviv'),
('Tarans', 'Guk', Null, 'taras@gmail.com.com', 33, 'M',  'Lubny');
go

--UNIQUE KEY constraint, insert not unique name&surname&midl_name
INSERT INTO [module_3 − Andrii Karankovskyi].[dbo].[client] 
(name, surname, midle_name, email, age, gender, city)
VALUES
('Ivan', 'Sobol', Null, 'ivan2@gmail.com', 30, 'M', 'Brody')

go

-- Insert corect data
INSERT INTO [module_3 − Andrii Karankovskyi].[dbo].[client_order] 
(product_name, product_description, price, quantity, product_weight, client_id, day_of_shipment)
VALUES
('Ковалда', 'Важкий залізний предмет', 155.65, 3, 65, 5, '2018.07.24'),
('Цегла', 'Важкий тупий предмет', 5, 1000, 70, 5, '2018.07.25'),
('Цвяхи', 'Залізний гострий предмет', 60.00, 100, 7.5, 3, '2018.07.24'),
('Яблука', 'Продукт харчування', 20.0, 1, 1, 1, '2018.07.30'),
('Курка', 'Жива птаха', 110.05, 11, 3, 2, '2018.07.29');

go

-- CHECK constraint, insert wrong gender
INSERT INTO [module_3 − Andrii Karankovskyi].[dbo].[client] 
(name, surname, midle_name, email, age, gender, city)
VALUES
('Крісло', 'Sobol', Null, 'крісло@gmail.com', 27, 'С', 'Харків')
go

-- FK constraint, insert wrong client_id
INSERT INTO [module_3 − Andrii Karankovskyi].[dbo].[client_order] 
(product_name, product_description, price, quantity, product_weight, client_id, day_of_shipment)
VALUES
('Плитка', 'Плитка керамічна', 155.65, 3, 65, 9, '2018.07.24')
go

SELECT * FROM dbo.[client_order]
go

SELECT * FROM dbo.[client]
go