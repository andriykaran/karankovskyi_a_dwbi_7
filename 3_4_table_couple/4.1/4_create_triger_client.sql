﻿use [module_3 − Andrii Karankovskyi]
go 


drop trigger if exists tr_client 
go

---- trigger creation
create trigger tr_client ON client
AFTER UPDATE
AS
BEGIN
--SET NOCOUNT ON;  set @@rowcount in 0
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation char(1)
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)
set @operation = 
case 
	when @ins > 0 and @del > 0 then 'U'  
	when @ins = 0 and @del > 0 then 'D'  
	when @ins > 0 and @del = 0 then 'I'  
end 

--- update
if @operation = 'U'
	begin
		update client 
		set updated_date = getdate()
		from client 
			inner join inserted on client.id = inserted.id		
	end

END
GO

--triger work check
UPDATE client 
SET midle_name = 'Semenovych'
WHERE name = 'Ivan'
go

SELECT * FROM client 
go

 