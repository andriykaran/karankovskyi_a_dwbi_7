﻿use [module_3 − Andrii Karankovskyi]
go

--- create view
CREATE OR ALTER VIEW client_view
  AS
  SELECT *
  FROM client
go


CREATE OR ALTER VIEW client_order_view
  AS
  SELECT *
  FROM client_order
  WITH CHECK OPTION
go

