﻿use [module_3 − Andrii Karankovskyi]
go 


drop trigger if exists tr_client_order
go

---- trigger creation
create trigger tr_client_order ON client_order
AFTER UPDATE
AS
BEGIN
--SET NOCOUNT ON;  set @@rowcount in 0
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation char(1)
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)
set @operation = 
case 
	when @ins > 0 and @del > 0 then 'U'  
	when @ins = 0 and @del > 0 then 'D'  
	when @ins > 0 and @del = 0 then 'I'  
end 

--- update
if @operation = 'U'
	begin
		update client_order 
		set updated_date = getdate()
		from client_order 
			inner join inserted on client_order.id = inserted.id		
	end

END
GO


--triger work check
UPDATE client_order 
SET product_name = 'Індик'
WHERE product_name = 'Качка'
go

SELECT * FROM client_order 
go

 