﻿use [education]
go 

DROP synonym if exists [A_Karankovskyi].[person]
go
DROP synonym if exists [A_Karankovskyi].[person_operation]
go

create synonym [A_Karankovskyi].person for [module_4 − Andrii Karankovskyi].[dbo].person
go

create synonym [A_karankovskyi].person_operation for [module_4 − Andrii Karankovskyi].[dbo].person_operation
go

