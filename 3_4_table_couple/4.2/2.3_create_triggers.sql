﻿use [module_4 − Andrii Karankovskyi]
go

drop trigger if exists tr_person 
go

---- trigger creation
create trigger dbo.tr_person ON dbo.person
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
--SET NOCOUNT ON;  set @@rowcount in 0
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation char(1)
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)
set @operation = 
case 
	when @ins > 0 and @del > 0 then 'U'  
	when @ins = 0 and @del > 0 then 'D'  
	when @ins > 0 and @del = 0 then 'I'  
end 
---- insert
if @operation = 'I'
begin
	insert into person_operation
				(name, surname, midle_name, email, age, gender, country, region, city, birthday, operation_type, operation_date)
		Select 
		inserted.name, inserted.surname, inserted.midle_name, inserted.email, inserted.age, inserted.gender, inserted.country, inserted.region, inserted.city, inserted.birthday, @operation, getdate()
	FROM person
	inner join inserted on person.id = inserted.id
  
end
---- delete
if @operation = 'D'
begin
	insert into person_operation  (name, surname, midle_name, email, age, gender, country, region, city, birthday, operation_type, operation_date)
	Select 
		deleted.name, deleted.surname, deleted.midle_name, deleted.email, deleted.age, deleted.gender, deleted.country, deleted.region, deleted.city, deleted.birthday, @operation,  getdate()
	FROM deleted 
end

--- update

if @operation = 'U'
	begin
		insert into person_operation  (name, surname, midle_name, email, age, gender, country, region, city, birthday, operation_type, operation_date)
		Select 
		deleted.name, deleted.surname, deleted.midle_name, deleted.email, deleted.age, deleted.gender, deleted.country, deleted.region, deleted.city, deleted.birthday, @operation,  getdate()
	FROM deleted 
	end

END
GO

