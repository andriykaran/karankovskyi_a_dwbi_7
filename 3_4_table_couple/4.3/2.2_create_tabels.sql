﻿use [module_4 − Andrii Karankovskyi]
go 

DROP TABLE if exists [person]
go

CREATE TABLE person(
	id int  NOT NULL PRIMARY KEY IDENTITY,
	name varchar(30) NOT NULL,
	surname varchar(20) NOT NULL,
	midle_name varchar(20),
	email varchar(50),
	age int, 
	gender varchar(1),
	country varchar(20),
	region varchar(20),
	city varchar(20),
	birthday date
	
	)
GO

DROP TABLE if exists [person_operation]
go

CREATE TABLE [person_operation](
	id int  NOT NULL PRIMARY KEY IDENTITY,
	name varchar(30) NOT NULL,
	surname varchar(20) NOT NULL,
	midle_name varchar(20),
	email varchar(50),
	age int, 
	gender varchar(1),
	country varchar(20),
	region varchar(20),
	city varchar(20),
	birthday date,
	operation_type char(1),
	operation_date datetime	
	)
GO
